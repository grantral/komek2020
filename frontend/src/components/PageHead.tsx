/**
 *
 * Page Head
 *
 */
import React from "react";
import { Helmet } from "react-helmet";

type Props = {
  title: string;
  description?: string;
};

const PageHead = (props: Props) => {
  const { title, description } = props;

  return (
    <Helmet>
      <title>{title} - Komek 2020</title>
      <meta content={description} name="description" />
    </Helmet>
  );
};

PageHead.defaultProps = {
  description: "Komek 2020 - Важен вклад каждого!",
};

export default PageHead;
