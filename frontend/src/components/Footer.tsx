import React from "react";
import { Container } from "reactstrap";

export const Footer = () => {
  return (
    <footer className="footer">
      <Container>
        <p className="text-center mb-0">Komek2020</p>
      </Container>
    </footer>
  );
};
