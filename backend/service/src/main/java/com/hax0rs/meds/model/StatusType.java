package com.hax0rs.meds.model;

public enum StatusType {
    OPEN,
    CLOSED,
    CANCELLED
}
