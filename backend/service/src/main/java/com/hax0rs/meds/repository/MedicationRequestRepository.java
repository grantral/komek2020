package com.hax0rs.meds.repository;

import com.hax0rs.meds.model.StatusType;
import com.hax0rs.meds.model.entity.MedicationOffer;
import com.hax0rs.meds.model.entity.MedicationRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicationRequestRepository extends JpaRepository<MedicationRequest, Long> {

    Page<MedicationRequest> findAllByStatus(StatusType statusType, Pageable pageable);

    Page<MedicationRequest> findByRequirementContainingIgnoreCaseAndStatus(String requirement, StatusType statusType, Pageable pageable);

    Page<MedicationRequest> findByStatus(StatusType statusType, Pageable pageable);

    Page<MedicationRequest> findByCityContainingIgnoreCaseAndStatus(String city, StatusType statusType, Pageable pageable);

    Page<MedicationRequest> findByCityContainingIgnoreCaseAndRequirementContainingIgnoreCaseAndStatus(String city, String requirement, StatusType statusType, Pageable pageable);

    MedicationRequest findOneById(Long id);
}
