package com.hax0rs.meds.repository;

import com.hax0rs.meds.model.StatusType;
import com.hax0rs.meds.model.entity.MedicationOffer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationOfferRepository extends JpaRepository<MedicationOffer, String> {

    Page<MedicationOffer> findAllByStatus(StatusType statusType, Pageable pageable);

    Page<MedicationOffer> findByOfferContainingIgnoreCaseAndStatus(String offer, StatusType statusType, Pageable pageable);

    Page<MedicationOffer> findByCityContainingIgnoreCaseAndStatus(String city, StatusType statusType, Pageable pageable);

    Page<MedicationOffer> findByCityContainingIgnoreCaseAndOfferContainingIgnoreCaseAndStatus(String city, String offer, StatusType statusType, Pageable pageable);

    MedicationOffer findOneById(Long id);
}
